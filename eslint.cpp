#include "eslint.h"

#include <kpluginfactory.h>
#include <kpluginloader.h>
#include <QAction>
#include <kactioncollection.h>
#include <ktexteditor/mainwindow.h>
#include <ktexteditor/view.h>
#include <ktexteditor/markinterface.h>
#include <kxmlguifactory.h>
#include <iostream>
#include <QFileInfo>
#include <QDir>
#include <QStringList>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <ktexteditor/message.h>

K_PLUGIN_FACTORY_WITH_JSON(
    EslintPluginFactory,
    "ktexteditor_eslint.json",
    registerPlugin<EslintPlugin>();
)

EslintPlugin::EslintPlugin(QObject *parent, const QVariantList &args)
    : KTextEditor::Plugin(parent)
{
    // Avoid warning on compile time because of unused argument
    Q_UNUSED(args);

    std::cout << "EslintPlugin constructor\n";
}

EslintPlugin::~EslintPlugin()
{
}

QObject* EslintPlugin::createView(KTextEditor::MainWindow *mainWindow)
{
    std::cout << "EslintPlugin::createView\n";
    EslintPluginView* nview = new EslintPluginView(this, mainWindow);
    return nview;
}

EslintPluginView::EslintPluginView(KTextEditor::Plugin *plugin, KTextEditor::MainWindow* mainWindow)
  : QObject(mainWindow)
  , m_mainWindow(mainWindow)
  , m_plugin(plugin)
  , m_markIface(NULL)
  , m_prevCursorLine(-1)
{
    KXMLGUIClient::setComponentName (QStringLiteral("ktexteditor_eslint"), i18n("eslint javascript"));
    setXMLFile( QStringLiteral("eslintui.rc") );

    QAction *a = actionCollection()->addAction(QStringLiteral("tools_do_eslint"));
    actionCollection()->setDefaultShortcut(a, QKeySequence(Qt::ALT + Qt::Key_L));
    a->setText(i18n("Do eslint"));
    connect(a, SIGNAL(triggered(bool)), this, SLOT(slotDoLint()));

    m_mainWindow->guiFactory()->addClient(this);
}

EslintPluginView::~EslintPluginView()
{
}

std::ostream& operator<<(std::ostream& os, const QString str)
{
    os << str.toUtf8().constData();
    return os;
}

QStringList& operator<<(QStringList& list, const int num)
{
    list << QString::number(num);
    return list;
}

void EslintPluginView::slotDoLint()
{
    std::cout << "EslintPluginView::slotDoLint\n";
    KTextEditor::Document* doc = m_mainWindow->activeView()->document();
    std::cout << doc->mode() << "\n";
    std::cout << doc->mimeType() << "\n";
    std::cout << doc->documentName() << "\n";
    std::cout << doc->url().toString() << "\n";

    const QString cfgPath = searchUp(".eslintrc.yml");
    const QFileInfo pkgInfo(doc->url().toLocalFile());
    const QString cwd(pkgInfo.absolutePath());
    std::cout << "Config path: " << cfgPath << '\n';
    std::cout << "Eslint working directory: " << cwd << '\n';
    startEslintProcess(cwd, cfgPath);
}

const QString EslintPluginView::searchUp(const QString fileName) const
{
    const KTextEditor::Document* doc = m_mainWindow->activeView()->document();
    const QUrl docUrl = doc->url();
    if (docUrl.isLocalFile()) {
        QFileInfo filepath(docUrl.toLocalFile());
        QDir dir(filepath.dir());
        while (dir.exists()) {
            //std::cout << "Searching " << fileName << " in " << dir.absolutePath() << '\n';
            if (dir.exists(fileName)) {
                return dir.absoluteFilePath(fileName);
            } else if(!dir.cdUp()) {
                break;
            }
        }
    }
    return QString();
}

void EslintPluginView::startEslintProcess(const QString cwd, const QString cfgPath)
{
    m_eslintProcess = new QProcess(this);
    m_eslintProcess->setProcessChannelMode(QProcess::SeparateChannels);
    m_eslintProcess->setWorkingDirectory(cwd);
    QStringList args;
    args << "--stdin" << "--config" << cfgPath << "--format" << "json";
    connect(m_eslintProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(eslintProcessFinished(int, QProcess::ExitStatus)));
    connect(m_eslintProcess, SIGNAL(started()), this, SLOT(eslintProcessStarted()));
    connect(m_eslintProcess, SIGNAL(errorOccurred(QProcess::ProcessError)), this, SLOT(eslintProcessErrorOccurred(QProcess::ProcessError)));
    m_eslintProcess->start("eslint", args);
}

void EslintPluginView::eslintProcessStarted()
{
    std::cout << "Eslint process started\n";
    const KTextEditor::Document* doc = m_mainWindow->activeView()->document();
    const QByteArray text(doc->text().toUtf8());
    m_eslintProcess->write(text);
    m_eslintProcess->closeWriteChannel();
}

void EslintPluginView::eslintProcessErrorOccurred(QProcess::ProcessError error)
{
    std::cout << "Eslint process error " << error << '\n';
    delete m_eslintProcess;
    m_eslintProcess = NULL;
}

void EslintPluginView::eslintProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    std::cout << "Eslint process finished with code " << exitCode << " and status " << exitStatus << '\n';
    const QByteArray result(m_eslintProcess->readAllStandardOutput());
    //std::cout << '\n' << result.constData() << '\n';
    QJsonParseError parseError;
    QJsonDocument data = QJsonDocument::fromJson(result, &parseError);
    if (parseError.error == QJsonParseError::NoError) {
        buildEslintItemList(data);
        showMarks();
    } else {
        std::cout << "Parse error " << parseError.error << " at offset " << parseError.offset << " : " << parseError.errorString() << '\n';
    }
    delete m_eslintProcess;
    m_eslintProcess = NULL;
}

/*
[{
    "filePath":"<text>",
    "messages":[
        {"ruleId":"no-unused-vars","severity":2,"message":"'libxmljs' is assigned a value but never used.","line":3,"column":7,"nodeType":"Identifier","source":"const libxmljs = require('libxmljs');"},
        {"ruleId":"import/no-unresolved","severity":2,"message":"Unable to resolve path to module 'libxmljs'.","line":3,"column":26,"nodeType":"Literal","source":"const libxmljs = require('libxmljs');"},
        {"ruleId":"space-before-blocks","severity":2,"message":"Missing space before opening brace.","line":5,"column":33,"nodeType":"BlockStatement","source":"function newDataObj(type, style){","fix":{"range":[86,86],"text":" "}},
        {"ruleId":"object-shorthand","severity":2,"message":"Expected property shorthand.","line":7,"column":9,"nodeType":"Property","source":"        type: type,","fix":{"range":[109,119],"text":"type"}},
        {"ruleId":"object-shorthand","severity":2,"message":"Expected property shorthand.","line":8,"column":9,"nodeType":"Property","source":"        style: style,","fix":{"range":[129,141],"text":"style"}},
        {"ruleId":"comma-dangle","severity":2,"message":"Missing trailing comma.","line":11,"column":22,"nodeType":"Property","source":"        objects: null","fix":{"range":[202,202],"text":","}},
        {"ruleId":"space-infix-ops","severity":2,"message":"Infix operators must be spaced.","line":16,"column":31,"nodeType":"BinaryExpression","source":"    if (libxmljsDomNode.type()!=='element') {","fix":{"range":[287,290],"text":" !== "}},
        {"ruleId":"quotes","severity":1,"message":"Strings must use singlequote.","line":17,"column":29,"nodeType":"Literal","source":"        throw new TypeError(\"Dom node should be an element\");","fix":{"range":[331,362],"text":"'Dom node should be an element'"}},
        {"ruleId":"no-use-before-define","severity":2,"message":"'parseElement' was used before it was defined.","line":19,"column":12,"nodeType":"Identifier","source":"    return parseElement(libxmljsDomNode, {"},{"ruleId":"array-bracket-spacing","severity":2,"message":"There should be no space after '['.","line":22,"column":15,"nodeType":"ArrayExpression","source":"        data: [ newDataObj('text') ]","fix":{"range":[479,480],"text":""}},
        {"ruleId":"array-bracket-spacing","severity":2,"message":"There should be no space before ']'.","line":22,"column":36,"nodeType":"ArrayExpression","source":"        data: [ newDataObj('text') ]","fix":{"range":[498,499],"text":""}},
        {"ruleId":"comma-dangle","severity":2,"message":"Missing trailing comma.","line":22,"column":37,"nodeType":"Property","source":"        data: [ newDataObj('text') ]","fix":{"range":[500,500],"text":","}},
        {"ruleId":"no-use-before-define","severity":2,"message":"'node' was used before it was defined.","line":27,"column":9,"nodeType":"Identifier","source":"    if (node.attr('class')) paraObj.style = node.attr('class');"},
        {"ruleId":"no-param-reassign","severity":2,"message":"Assignment to property of function parameter 'paraObj'.","line":27,"column":29,"nodeType":"Identifier","source":"    if (node.attr('class')) paraObj.style = node.attr('class');"},
        {"ruleId":"no-use-before-define","severity":2,"message":"'node' was used before it was defined.","line":27,"column":45,"nodeType":"Identifier","source":"    if (node.attr('class')) paraObj.style = node.attr('class');"},
        {"ruleId":"default-case","severity":2,"message":"Expected a default case.","line":31,"column":9,"nodeType":"SwitchStatement","source":"        switch (node.type()) {"},{"ruleId":"no-fallthrough","severity":2,"message":"Expected a 'break' statement before 'case'.","line":34,"column":13,"nodeType":"SwitchCase","source":"            case 'element':"},{"ruleId":"space-infix-ops","severity":2,"message":"Infix operators must be spaced.","line":41,"column":45,"nodeType":"BinaryExpression","source":"                        if (lastData.objects!==null) {","fix":{"range":[1221,1224],"text":" !== "}},
        {"ruleId":"prefer-const","severity":2,"message":"'obj' is never reassigned. Use 'const' instead.","line":42,"column":38,"nodeType":"Identifier","source":"                            for (let obj of lastData.objects) {","fix":{"range":[1265,1268],"text":"const"}},{"ruleId":"no-unused-vars","severity":2,"message":"'obj' is assigned a value but never used.","line":42,"column":38,"nodeType":"Identifier","source":"                            for (let obj of lastData.objects) {"},
        {"ruleId":"no-undef","severity":2,"message":"'dataObj' is not defined.","line":43,"column":37,"nodeType":"Identifier","source":"                                if (dataObj.type==='ref') dataObj.index += 1;"},
        {"ruleId":"space-infix-ops","severity":2,"message":"Infix operators must be spaced.","line":43,"column":49,"nodeType":"BinaryExpression","source":"                                if (dataObj.type==='ref') dataObj.index += 1;","fix":{"range":[1344,1347],"text":" === "}},
        {"ruleId":"no-undef","severity":2,"message":"'dataObj' is not defined.","line":43,"column":59,"nodeType":"Identifier","source":"                                if (dataObj.type==='ref') dataObj.index += 1;"},
        {"ruleId":"comma-dangle","severity":2,"message":"Missing trailing comma.","line":52,"column":37,"nodeType":"Property","source":"                            index: 0","fix":{"range":[1735,1735],"text":","}},
        {"ruleId":"no-case-declarations","severity":2,"message":"Unexpected lexical declaration in case block.","line":55,"column":21,"nodeType":"SwitchCase","source":"                    case 'span':"},
        {"ruleId":"no-case-declarations","severity":2,"message":"Unexpected lexical declaration in case block.","line":55,"column":21,"nodeType":"SwitchCase","source":"                    case 'span':"},
        {"ruleId":"object-shorthand","severity":2,"message":"Expected property shorthand.","line":60,"column":29,"nodeType":"Property","source":"                            style: style","fix":{"range":[2051,2063],"text":"style"}},
        {"ruleId":"comma-dangle","severity":2,"message":"Missing trailing comma.","line":60,"column":41,"nodeType":"Property","source":"                            style: style","fix":{"range":[2063,2063],"text":","}},
        {"ruleId":"comma-dangle","severity":2,"message":"Missing trailing comma.","line":81,"column":20,"nodeType":"Property","source":"    createParagraph","fix":{"range":[2693,2693],"text":","}}],
    "errorCount":28,
    "warningCount":1,
    "source":"..."
}]
*/

void EslintPluginView::buildEslintItemList(const QJsonDocument data)
{
    if (!data.isArray()) {
        std::cout << "JSON should be an Array\n";
        return;
    }
    
    m_list.clear();

    //files array
    const QJsonArray array(data.array());
    QJsonArray::const_iterator it0(array.begin());
    for (; it0!=array.end(); it0++) {
        const QJsonValue value(*it0);
        if (value.isObject()) {
            const QJsonObject obj(value.toObject());
            const QJsonValue messages(obj.value("messages"));
            if (messages.isArray()) {
                const QJsonArray messageArray(messages.toArray());
                QJsonArray::const_iterator it1(messageArray.begin());
                for(; it1!=messageArray.end(); it1++){
                    const QJsonValue message(*it1);
                    if (message.isObject()) {
                        const EslintItem item(message.toObject());
                        m_list.append(item);
                    } else {
                        std::cout << "Message array element should be an object\n";
                    }
                }
            } else {
                std::cout << "Messages field should be an array\n";
            }
        } else {
            std::cout << "File array element should be an Object\n";
        }
    }
}

void EslintPluginView::showMarks()
{
    const KTextEditor::Document* doc = m_mainWindow->activeView()->document();
    KTextEditor::MarkInterface* markIface = qobject_cast<KTextEditor::MarkInterface*>(doc);
    if (!markIface) {
        std::cout << "fatal: KTextEditor::Document doesnt implement mark interface";
        return;
    }

    if (m_markIface) {
        QList<KTextEditor::View*> views(m_markIface->views());
        QList<KTextEditor::View*>::const_iterator it(views.begin());
        for (; it!=views.end(); it++) {
            disconnect(*it, SIGNAL(cursorPositionChanged(KTextEditor::View*, const KTextEditor::Cursor&)), this, SLOT(cursorPositionChanged(KTextEditor::View*, const KTextEditor::Cursor&)));
        }
    }
    {
        QList<KTextEditor::View*> views(doc->views());
        QList<KTextEditor::View*>::const_iterator it(views.begin());
        for (; it!=views.end(); it++) {
            connect(*it, SIGNAL(cursorPositionChanged(KTextEditor::View*, const KTextEditor::Cursor&)), this, SLOT(cursorPositionChanged(KTextEditor::View*, const KTextEditor::Cursor&)));
        }
        m_markIface = doc;
    }

    markIface->clearMarks();
    m_map.clear();

    QVector<EslintItem>::const_iterator it(m_list.begin());
    for (; it!=m_list.end(); it++) {
        const EslintItem& item = *it;
        markIface->setMark(item.line, item.type);
    }
    const QHash<int, KTextEditor::Mark*>& marks = markIface->marks();
    for (int i=0; i<m_list.size(); i++) {
        const EslintItem& item = m_list[i];
        const KTextEditor::Mark* const mark = marks.value(item.line);
        m_map.insertMulti(mark, i);
    }
}

void EslintPluginView::cursorPositionChanged(KTextEditor::View *view, const KTextEditor::Cursor &newPosition)
{
    std::cout << newPosition.line() << " cursorPositionChanged\n";
    if (m_prevCursorLine == newPosition.line()) return;
    KTextEditor::Document* doc = view->document();
    KTextEditor::MarkInterface* markIface = qobject_cast<KTextEditor::MarkInterface*>(doc);
    if (markIface) {
        QStringList text;
        const KTextEditor::Mark* const mark = markIface->marks().value(newPosition.line(), NULL);
        QHash<const KTextEditor::Mark* const, int>::const_iterator it(m_map.find(mark));
        for (; it!=m_map.end() && it.key()==mark; it++) {
            const EslintItem& item = m_list[*it];
            switch (item.type) {
                case KTextEditor::MarkInterface::Warning:
                    text << "Warning: ";
                    break;
                case KTextEditor::MarkInterface::Error:
                    text << "Error: ";
                    break;
            }
            text << item.ruleId << ", " << item.nodeType << ", " << item.message << "\n    ";
            text << (newPosition.line() + 1) << ":" << item.column << " " << item.source << "\n";
        }
        if (m_activeMessage) delete m_activeMessage;
        if (text.size()) {
            m_activeMessage = new KTextEditor::Message(text.join(""), KTextEditor::Message::Positive);
            m_activeMessage->setWordWrap(false);
            m_activeMessage->setPosition(KTextEditor::Message::TopInView);
            doc->postMessage(m_activeMessage);
        }
    }
}

EslintItem::EslintItem(const QJsonObject obj)
{
    line = ((int) obj.value("line").toDouble()) - 1;
    column = (int) obj.value("column").toDouble();
    switch ((int) obj.value("severity").toDouble()) {
        case 1:
            type = KTextEditor::MarkInterface::Warning;
            break;
        case 2:
            type = KTextEditor::MarkInterface::Error;
            break;
        default:
            type = KTextEditor::MarkInterface::markType32;
    }
    ruleId = obj.value("ruleId").toString();
    message = obj.value("message").toString();
    nodeType = obj.value("nodeType").toString();
    source = obj.value("source").toString();
    const QJsonValue fixValue(obj.value("fix"));
    if (fixValue.isObject()) {
        const QJsonObject fixObj(fixValue.toObject());
        const QJsonArray fixRange(fixObj.value("range").toArray());
        fixStartPosition = (int)fixRange[0].toDouble();
        fixEndPosition = (int)fixRange[1].toDouble();
        fixText = fixObj.value("text").toString();
    } else {
        fixStartPosition = fixEndPosition = -1;
        fixText = QString();
    }
}

EslintItem::EslintItem()
{
}

EslintItem::EslintItem(const EslintItem& other)
{
    line = other.line;
    column = other.column;
    type = other.type;
    ruleId = other.ruleId;
    message = other.message;
    nodeType = other.nodeType;
    source = other.source;
    fixStartPosition = other.fixStartPosition;
    fixEndPosition = other.fixEndPosition;
    fixText = other.fixText;
}

EslintItem::~EslintItem()
{
}

#include "eslint.moc"
