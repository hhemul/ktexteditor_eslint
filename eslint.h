#ifndef ESLINT_H
#define ESLINT_H

#include <ktexteditor/plugin.h>
#include <kxmlguiclient.h>
#include <klocalizedstring.h>
#include <QtCore/QObject>
#include <QtCore/QProcess>
#include <QtCore/QHash>
#include <QtCore/QVector>
#include <QtCore/QPoint>
#include <QtCore/QPointer>

namespace KTextEditor {
    class MainWindow;
    class Document;
    class View;
    class Cursor;
    class Message;
    class Mark;
}
class QJsonObject;

class EslintPlugin
  : public KTextEditor::Plugin
{
    Q_OBJECT
  public:

    explicit EslintPlugin(QObject *parent, const QVariantList &args);
    virtual ~EslintPlugin();

    QObject* createView(KTextEditor::MainWindow *mainWindow);
};

class EslintItem
{
public:
    EslintItem();
    explicit EslintItem(const QJsonObject obj);
    EslintItem(const EslintItem& other);
    ~EslintItem();

    int line;
    int column;
    unsigned int type;
    QString ruleId;
    QString message;
    QString nodeType;
    QString source;
    int fixStartPosition, fixEndPosition;
    QString fixText;
};

Q_DECLARE_TYPEINFO(EslintItem, Q_MOVABLE_TYPE);

class EslintPluginView
   : public QObject, public KXMLGUIClient
{
    Q_OBJECT

  public:
    explicit EslintPluginView(KTextEditor::Plugin *plugin, KTextEditor::MainWindow* mainWindow);
    ~EslintPluginView();

  private Q_SLOTS:
    void slotDoLint();
    void eslintProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void eslintProcessErrorOccurred(QProcess::ProcessError error);
    void eslintProcessStarted();
    void cursorPositionChanged(KTextEditor::View *view, const KTextEditor::Cursor &newPosition);

  private:
    QVector<EslintItem> m_list;
    QHash<const KTextEditor::Mark* const, int> m_map;
    KTextEditor::MainWindow* m_mainWindow;
    KTextEditor::Plugin* m_plugin;
    const KTextEditor::Document* m_markIface;
    QPointer<KTextEditor::Message> m_activeMessage;
    int m_prevCursorLine;
    QProcess* m_eslintProcess;

    const QString searchUp(const QString fileName) const;
    void startEslintProcess(const QString cwd, const QString cfgPath);
    void buildEslintItemList(const QJsonDocument data);
    void showMarks();
};

#endif
