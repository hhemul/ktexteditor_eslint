find_package(ECM 0.0.11 REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings)
include(FeatureSummary)

find_package(Qt5 REQUIRED COMPONENTS Widgets)
find_package(KF5 REQUIRED COMPONENTS CoreAddons Solid)
find_package(KF5TextEditor)

set(ktexteditor_eslint_src eslint.cpp)

qt5_add_resources(ktexteditor_eslint_src plugin.qrc)

add_library(ktexteditor_eslint MODULE ${ktexteditor_eslint_src})

kcoreaddons_desktop_to_json (ktexteditor_eslint ktexteditor_eslint.desktop)

target_link_libraries(ktexteditor_eslint KF5::TextEditor KF5::WidgetsAddons)

# Well, we want to install our plugin on the plugin directory
#install(TARGETS ktexteditor_eslint DESTINATION ${PLUGIN_INSTALL_DIR}/ktexteditor)
install(TARGETS ktexteditor_eslint DESTINATION /usr/lib/x86_64-linux-gnu/qt5/plugins/ktexteditor)

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
